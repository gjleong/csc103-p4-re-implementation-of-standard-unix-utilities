#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";




int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
	// use a map of <string, int>
	// for every string
	// if duplicate exists increase count via value of KV pair
	
	//map<string, int> unique_entries;
	string new_input, old_input;
	int count = 0;

	while (cin >> new_input){
		//input is already sorted
		old_input = new_input;
		if (old_input == new_input){
			count++;
		}
		else {
			if (showcount == 0 && dupsonly == 0 && uniqonly == 0)
				goto no_flags;
			if (showcount == 1){
				//cout << "Count for each line: \n";
				goto line_count;
			}
return_from_count:
			if (dupsonly == 1){
				//cout << "Duplicate lines only: \n";
				goto dupes;
			}
			else if (uniqonly == 1){
				//cout << "Unique lines only: \n";
				goto unique;
			}
return_to_loop:
			count = 0;
		}
	}
	goto end_if_else;

line_count:
	// for -c [count]
	cout << count << ": " << old_input << "\n";
	goto return_from_count;
dupes:
	// for -d [duplicates]
	if (count > 1)
		cout << old_input;
	if (uniqonly == 1)
		goto unique;
	goto return_to_loop;
unique:
	// for -u [unique lines]
	if (count == 1)
		cout << old_input;
	goto return_to_loop;

no_flags:
	cout << old_input;
	goto return_to_loop;

end_if_else:
	return 0;
}
